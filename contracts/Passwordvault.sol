pragma solidity ^0.5.0;
contract Passwordvault{
uint VaultCount = 1;

struct Vault {
string uname;
string pswd;
string webs;
}

event CUDEvent(bytes1 OpCode, uint len);


mapping (address => uint) vaultCode;
mapping (uint => Vault[] ) mVault;

function getVaultInfo() public view returns(uint vc, uint len) {

//require( vaultCode[msg.sender] >0 , "No entry present" );

if(vaultCode[msg.sender] >0)
{
vc = vaultCode[msg.sender];
len = mVault[vc].length;
}
else
{
vc = 0;
len = 0;
}

}

function fetchVault(uint vNo, uint index) public view returns (string memory uname, string memory pswd, string memory webs){
// require(vaultCode[msg.sender] == vNo, "Not your vault");
if ( vaultCode[msg.sender] == vNo )
{
if (mVault[vNo].length <= index )
{
return ( "Bad index", "" , "" );
}

uname = mVault[vNo][index].uname;
pswd = mVault[vNo][index].pswd;
webs = mVault[vNo][index].webs;

//return ( mVault[vNo][index].uname, mVault[vNo][index].pswd ,mVault[vNo][index].webs );
}
else
{
return ("Sorry not your account", "", "");
}
}

function addCred(string memory uname, string memory pswd, string memory webs) public returns(bool){
Vault memory details;

details.uname = uname;
details.pswd = pswd;
details.webs= webs;
if( vaultCode[msg.sender] == 0 )
{
vaultCode[msg.sender] = VaultCount;
mVault[VaultCount].push(details);
++VaultCount;
}else
{

mVault[vaultCode[msg.sender]].push(details) ;

}

emit CUDEvent('C', mVault[vaultCode[msg.sender]].length);

return (true);

}

function editCred(uint index, string memory pswd) public returns(bool){
 
   // require(pswd[0] != '');
    //require(index >= 0);
 
    mVault[vaultCode[msg.sender]][index].pswd = pswd;
    
    emit CUDEvent('U', mVault[vaultCode[msg.sender]].length);
    
    return true;
}



}